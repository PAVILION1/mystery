#include <iostream>
#include <vector>
using namespace std;

void print(auto A)
{
   for (auto a : A) 
        cout <<a<<" ";

   cout<<endl;
}

void mystery1(auto& Data)
{
  cout<<endl<<"Mystery 1"<<endl<<"---------------------"<<endl;

  for ( int i = 0 ; i < Data.size( ) ; i++)
  {
    for ( int j = 0 ; j < i ; j++)
	if ( Data[ i ] < Data[ j ] )
	    swap( Data[ i ] , Data[ j ] );

    print(Data);
  }//end outer for (this brace is needed to include the print statement)

}

void mystery2(auto& Data)
{
	cout<<endl<<"Mystery 2"<<endl<<"---------------------"<<endl;
	
	for (int k = 1; k < Data.size(); k++)
	{
		for (int i = 0; i < Data.size() -1 - k; i++)
			if (Data[i] > Data[i +1])
               swap(Data[i], Data[i + 1]);
               
         print(Data);
     }          
}
	
void mystery3(auto& Data)
{
	cout<<endl<<"Mystery 3"<<endl<<"---------------------"<<endl;
	
	int nextIndex, moveitem, insertVal;


for (nextIndex = 1; nextIndex < Data.size(); nextIndex ++)
{

insertVal = Data[nextIndex]; 
 moveitem = nextIndex; 

while (moveitem > 0 && Data[moveitem - 1] > insertVal)
{

Data[moveitem ] = Data[moveitem - 1];
 moveitem--;

} 

 Data[moveitem] = insertVal;
 
 print(Data);
 } 
	
}
//... Other mysteries...

int main()
{
    
  vector<int> Data = {36, 18, 22, 30, 29, 25, 12};

  vector<int> D1 = Data;
  vector<int> D2 = Data;
  vector<int> D3 = Data;

  mystery1(D1);
  mystery2(D2);
  mystery3(D3);


}
